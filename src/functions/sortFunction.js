const checkSortStatus = (list, status) => {
    const compare = (v1, v2) => {
        return v1 > v2 ? 1 : (v1 < v2) ? -1 : 0;
    }
    if (typeof status[0] === 'number') {
        return status[0] === 1
            ? list.sort((a, b) => compare(a.volumeInfo.title.toUpperCase(), b.volumeInfo.title.toUpperCase()))
            : list.sort((a, b) => compare(b.volumeInfo.title.toUpperCase(), a.volumeInfo.title.toUpperCase()))
    }
    if (typeof status[1] === 'number') {
        return status[1] === 1
            ? list.sort((a, b) => compare(a.volumeInfo.publishedDate, b.volumeInfo.publishedDate))
            : list.sort((a, b) => compare( b.volumeInfo.publishedDate, a.volumeInfo.publishedDate))
    }
    if (typeof status[2] === 'number') {
        return status[2] === 1
            ? list.sort((a, b) => compare(a.volumeInfo.title.length, b.volumeInfo.title.length))
            : list.sort((a, b) => compare( b.volumeInfo.title.length, a.volumeInfo.title.length))
    }
    return list;
}
export default checkSortStatus;