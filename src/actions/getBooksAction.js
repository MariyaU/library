export const searchResult = (booksCollection) => {
    return {
        type: 'ADD_BOOKS_COLLECTION',
        data: booksCollection
    };
};

export const deleteBooksList = () => {
    return {
        type: 'DELETE_BOOKS_LIST'
    };
};

export const uniquePublisherCollection = (booksCollection) => {
    let publisher = booksCollection.map(item => {
        return item.volumeInfo.publisher;
    });
    return {
        type: 'UNIQUE_COLLECTION_PUBLISHER',
        data: publisher
    }
};

export const uniqueYearCollection = (booksCollection) => {
    let years = booksCollection.map(item => {
        return item.volumeInfo.publishedDate;
    });
    return {
        type: 'UNIQUE_COLLECTION_YEAR',
        data: years
    }
};

export const clearUniguePublisherCollection = () => {
    return {
        type: 'CLEAR_UNIQUE_PUBLISHER'
    }
};

export const clearUnigueYearCollection = () => {
    return {
        type: 'CLEAR_UNIQUE_YEAR'
    }
};