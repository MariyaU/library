import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers/reducer';

const saveToLocalStorage = (state) => {
  try {
    const actualState = JSON.stringify(state);
    localStorage.setItem('state', actualState)
  } catch(err) {
    console.log(err)
  }
}

const loadStateFromLocalStorage = () => {
  // localStorage.clear();
  try {
    const actualState = localStorage.getItem('state');
    if (actualState === null) {
      return undefined
    }
    return JSON.parse(actualState);
  } catch(err) {
    console.log(err);
    return undefined;
  }
}

const presistedState = loadStateFromLocalStorage();

const store = createStore(
  reducer,
  presistedState
  );

  store.subscribe(() => saveToLocalStorage(store.getState()))

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
