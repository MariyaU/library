import React from 'react';
import { connect } from 'react-redux';
import star from '../../img/star.png';
import remove from '../../img/delete.png';
import { deleteFavouriteBook } from '../../actions/favouriteBooksAction';
import noImg from '../../img/noimage.png';


const FavouriteBooksList = (props) => {
    const handleDelete = (e) => {
        e.preventDefault();
        props.deleteFavouriteBook(e.target.id);
    }
    return (
        <div className='container'>
            <h2 className='mt-4'>Избранное</h2>
            <div className='row mt-4'>
                {props.favouriteBooksList.length ? props.favouriteBooksList.map((book, index) => {
                    return (
                        <div key={index} className='col-3 card-deck mb-4'>
                            <div className='card'>
                                <img className="card-img-top" src={book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.thumbnail : noImg} alt={book.title} />
                                <div className='card-body'>
                                    <span className='card-title font-weight-normal'>{book.volumeInfo.title.split('/')[0]}</span>
                                    <div className='card-text font-weight-light'><img className='mb-1 mr-1' src={star} alt='rating' />{book.volumeInfo.title.length}</div>
                                    {book.volumeInfo.authors ? <div className='card-text font-weight-light'>Авторы: {book.volumeInfo.authors && book.volumeInfo.authors.join(', ')}</div> : null}
                                    <div><a href={book.volumeInfo.infoLink}>Подробнее</a></div>
                                    <a href="#" onClick={handleDelete}><img src={remove} id={book.id} alt='delete' /></a>
                                </div>
                            </div>
                        </div>
                    )
                }) : <div className='container text-muted'>Пока ничего не выбрано</div>
                }
                
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        favouriteBooksList: state.favouriteBooks
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        deleteFavouriteBook: (id) => dispatch(deleteFavouriteBook(id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FavouriteBooksList);