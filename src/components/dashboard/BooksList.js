import React, { Component } from 'react';
import { connect } from 'react-redux';
import checkSortStatus from '../../functions/sortFunction';
import star from '../../img/star.png';
import { favouriteBooks } from '../../actions/favouriteBooksAction';
import noImg from '../../img/noimage.png';

class BooksList extends Component {

    handleClick = (e) => {
        e.preventDefault();
        this.props.addFavourite(this.props.booksCollection, e.target.id);
    }

    render() {
        let filterPublisher = this.props.booksCollection.filter(item => item.volumeInfo.publisher === this.props.filterPublisher);
        let filterYear = this.props.booksCollection.filter(item => parseInt(item.volumeInfo.publishedDate) === +this.props.filterYear);
        let listBooks = (this.props.filterPublisher && this.props.filterYear) ? (
            filterPublisher.filter(item => parseInt(item.volumeInfo.publishedDate) === +this.props.filterYear)
        )
            : (this.props.filterPublisher) ? (
                filterPublisher
            ) : (this.props.filterYear) ? (filterYear)
                    : (this.props.booksCollection);
        listBooks = checkSortStatus(listBooks, this.props.sortStatus);
        return (
            <div className='row mt-4'>
                {this.props.searchError ? <div className='container'>Мы ничего не нашли ¯\_(ツ)_/¯</div> : listBooks ? listBooks.map((book, index) => {
                    return (
                        <div key={index} className='card-deck col-sm-6 mb-4'>
                            <div className='card'>
                                <img className="card-img-top" src={book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.thumbnail : noImg} alt={book.title} />

                                <div className='card-body'>
                                    <span className='card-title font-weight-normal'>{book.volumeInfo.title.split('/')[0]}</span>
                                    <div className='card-text font-weight-light'><img className='mb-1 mr-1' src={star} alt='rating' />{book.volumeInfo.title.length}</div>
                                    {book.volumeInfo.authors ? <div className='card-text font-weight-light'>Авторы: {book.volumeInfo.authors && book.volumeInfo.authors.join(', ')}</div> : null}
                                    <div>
                                        <a className='mr-1' href="#" onClick={this.handleClick} id={book.id}><div id={book.id} className='mt-1 backgroung-like'></div></a>
                                    </div>
                                    <a href={book.volumeInfo.infoLink}>Подробнее</a>

                                </div>
                            </div>
                        </div>
                    )
                })
                    : <div>Мы ничего не нашли</div>
                }
            </div>

        )
    }
}
const mapStateToProps = (state) => {
    return {
        booksCollection: state.booksCollection,
        filterPublisher: state.filterPublisher,
        filterYear: state.filterYear,
        sortStatus: state.sortStatus,
        favouriteBooks: state.favouriteBooks,
        searchError: state.searchError
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addFavourite: (arr, id) => dispatch(favouriteBooks(arr, id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(BooksList);