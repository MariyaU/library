const searchErrorReducer = (state = false, action) => {
    switch (action.type) {
        case 'SET_SEARCH_ERROR':
            return true;
        case 'DELETE_SEARCH_ERROR':
            return false;
        default:
            return state;
    }
}

export default searchErrorReducer;