const uniqueKeysYearReducer = (state = [], action) => {
    switch (action.type) {
        case 'UNIQUE_COLLECTION_YEAR': {
            const getUnique = (arr) => {
                let result = [];
                for (let str of arr) {
                    if (!result.includes(parseInt(str)) && parseInt(str)) {
                        result.push(parseInt(str));
                    }
                }
                return result;
            }
            let uniqueYear = getUnique(action.data).sort(function(a, b) {
                return a - b;
            });
            return uniqueYear;
        }
        case 'CLEAR_UNIQUE_YEAR': {
            return [];
        }
        default:
            return state;
    }
}

export default uniqueKeysYearReducer;